function openSlideMenu() {
  if (window.matchMedia('(min-width:550px)').matches) {
    document.getElementById('side-menu').style.width = '638px';
  } else {
    document.getElementById('side-menu').style.width = '100%';
  }
  document.querySelector('.hamburger').style.visibility = 'hidden';
  document.querySelector('.close-menu').style.visibility = 'visible';
}
function closeSlideMenu() {
  document.getElementById('side-menu').style.width = '0';
  document.querySelector('.hamburger').style.visibility = 'visible';
  document.querySelector('.close-menu').style.visibility = 'hidden';
}

const link = document.getElementsByTagName('li');

for (i = 0; i < link.length; ++i) {
  link[i].addEventListener('click', () => {
    closeSlideMenu();
  });
}
