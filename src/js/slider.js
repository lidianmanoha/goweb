const slides = document.querySelector('.slider').children;
const dot = document.querySelector('.dots');
const dotParent = document.querySelector('.dots-container');
let index = 0;

// Change slide
function changeSlide() {
  for (let i = 0; i < slides.length; i++) {
    slides[i].classList.remove('active');
  }

  slides[index].classList.add('active');
}

// create Dots
function dotsIndicator() {
  for (let i = 0; i < slides.length; i++) {
    const div = document.createElement('div');
    // div.innerHTML = i + 1;
    div.setAttribute('onclick', 'indicateSlide(this)');
    div.id = i;
    if (i == 0) {
      div.className = 'active';
    }
    dot.appendChild(div);
  }
}
dotsIndicator();



function indicateSlide(element) {
  index = element.id;
  changeSlide();
  updateDotIndicator();
  resetTimer();
}

// Update dots
function updateDotIndicator() {
  for (let i = 0; i < dot.children.length; i++) {
    dot.children[i].classList.remove('active');
  }
  dot.children[index].classList.add('active');
}

//  Preview & next slide
function prevSlide() {
  if (index == 0) {
    index = slides.length - 1;
  } else {
    index--;
  }
  changeSlide();
}

function nextSlide() {
  if (index == slides.length - 1) {
    index = 0;
  } else {
    index++;
  }
  changeSlide();
}

const time = 4000;
// Reset Timer after click dot
function resetTimer() {
  clearInterval(timer);
  timer = setInterval(autoPlay, time);
}

// Autoplay Slider
function autoPlay() {
  nextSlide();
  updateDotIndicator();
}

let timer = setInterval(autoPlay, time);
