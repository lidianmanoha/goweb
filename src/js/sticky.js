const sticky = document.querySelector('.sticky-aside').getBoundingClientRect();
const foot = document.querySelector('footer').getBoundingClientRect();
const stick = document.querySelector('.sticky-aside');

const stickyTop = sticky.top;
const footTop = foot.top;
const margin = 0;
offtop = stickyTop - 188;
offbtm = footTop - (stickyTop * 2 + margin + sticky.height + 188);

document.addEventListener('scroll', function activateScroll() {
  if (window.matchMedia('screen and (min-width:768px)').matches) {
    /*Activation de la position fixe en scrollant de haut en bas*/
    if (window.pageYOffset > offtop && stick.classList.contains('top')) {
      stick.classList.remove('top');
      stick.classList.add('fixed');
      stick.style.animation = 'smoothScroll 2s forwards';
      // stick.style.marginTop = margin + 'px';

      /*Désactivation de la position fixe en scrollant de bas en haut*/
    }
    if (offtop >= window.pageYOffset && stick.classList.contains('fixed')) {
      stick.classList.remove('fixed');
      stick.style.animation = 'smoothScrollUp 2s forwards';
      stick.classList.add('top');
      // stick.style.marginTop = 'auto';

      /*Désactivation de la position fixe en scrollant de haut en bas*/
    }
    if (window.pageYOffset > offbtm && stick.classList.contains('fixed')) {
      stick.classList.remove('fixed');
      stick.style.animation = 'smoothScrollDown 2s forwards';
      stick.classList.add('bottom');
      stick.style.marginTop = offbtm - margin + 'px';

      /*Activation de la position fixe en scrollant de bas en haut*/
    }
    if (offbtm > window.pageYOffset && stick.classList.contains('bottom')) {
      stick.classList.remove('bottom');
      stick.classList.add('fixed');
      stick.style.marginTop = 0;

      stick.style.animation = 'smoothScrollUp 2s forwards';
    }
  }
});

